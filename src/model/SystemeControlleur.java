package model;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import view.View;

public class SystemeControlleur {

	private int etageCourant;
	private int etageCible;
	private Appel prochain;
	private SensDepEnum sensDeplacement;
	private int position_exacte;
	private Ordonnanceur ordonnanceur;
	private Cabine cabine;
	private view.Cabine cabineVue;
	private view.CapteurEtage capteur_cabine;
	private Porte porte;
	private view.Porte porteVue;
	private boolean ok;
	private boolean ok2;
	private HashMap<model.CapteurEtage, view.CapteurEtage> capteurs;
	private CapteurEtage modelCapteurEtag_avant;
	private CapteurEtage modelCapteurEtage_final;
	private boolean arret_urgence;
	private JButton arret_urgence_btn;

	public SystemeControlleur() {

		ordonnanceur = new Ordonnanceur(this);
		etageCourant = 0;
		position_exacte = 672;
		sensDeplacement = SensDepEnum.ARRET;
		prochain = null;
		View view = new View(this);
		ok = false;
		ok2 = false;
		arret_urgence = false;
		cabine = new Cabine(this);
		cabineVue = view.getCabine();
		capteur_cabine = view.getCapteur_cabine();
		view.getLblListeOrdo();
		arret_urgence_btn = view.getBtnNewButton_arret_urgence();

		porte = new Porte();
		porteVue = view.getPorte();

		this.capteurs = new HashMap<model.CapteurEtage, view.CapteurEtage>();
		int i = 0;
		for (view.CapteurEtage capteurEtage : view.getCapteurs()) {

			this.capteurs.put(new model.CapteurEtage(i), capteurEtage);
			i++;

		}

	}

	/**
	 * Fonction qui permet d'enlever un étage de l'ordonnanceur et de s'y
	 * rendre, de manière simple.
	 */
	public void controle() {
		
		verify_presence(); // vérifier les capteurs.
		//update_liste_view();

		// si on peut essayer d'aller a un étage car on est pas occupé à bouger
		// ou avec les portes ouvertes

		if (ok && ok2 && !porte.isStatus()) {
			// si on est arrivé

			// ouvrir les portes + temp
			porteVue.set_open();
			porte.ouvrir();
			porte.maintenir();
			porteVue.set_close();
			porte.fermer();
			ok = false;
			ok2 = false;

		}

		if (!(sensDeplacement == SensDepEnum.ARRET))
			ok2 = true;

		else if (sensDeplacement == SensDepEnum.ARRET && !porte.isStatus()) {


			// Si on est arrivé a destination
			if (etageCourant == etageCible) {
				ok = true; // on demande l'ouverture des portes
				this.ouvrir_porte();
				// puis le nouvel étage à aller.
				if (!ordonnanceur.getProchaineDepMontee().isEmpty() || !ordonnanceur.getProchaineDepDesc().isEmpty()) {
					prochain = ordonnanceur.recupererAppel();
					etageCible = prochain.getEtage(); // remplacer par l'appel
					// donné par
					// l'ordonnanceur.
					System.out.println("Nouvel etage cible: " + etageCible);
					if (etageCible == etageCourant) {
						ok2 = true;
						this.ouvrir_porte();
					}

				}

			}

			else {
				ok = false;

				// permet d'obtenir les bons capteurs a repérer
				if (etageCible > etageCourant) {
					for (model.CapteurEtage modelCapteurEtage : capteurs.keySet()) {
						if (modelCapteurEtage.getEtage() == etageCible - 1)
							this.modelCapteurEtag_avant = modelCapteurEtage;
						if (modelCapteurEtage.getEtage() == etageCible)
							this.modelCapteurEtage_final = modelCapteurEtage;

					}
				} else if (etageCible < etageCourant) {
					for (model.CapteurEtage modelCapteurEtage : capteurs.keySet()) {
						if (modelCapteurEtage.getEtage() == etageCible + 1)
							this.modelCapteurEtag_avant = modelCapteurEtage;
						if (modelCapteurEtage.getEtage() == etageCible)
							this.modelCapteurEtage_final = modelCapteurEtage;

					}
				}

				// et on bouge
				if (!arret_urgence)
					bouger_cabine();
			}

		}
	}

	// permet de commander les déplacement de la cabine.
	public void bouger_cabine() {

		if (etageCible - 1 > this.etageCourant) {
			cabine.monter(modelCapteurEtag_avant);
			sensDeplacement = SensDepEnum.MONTEE;

		}

		else if (etageCible + 1 < this.etageCourant) {
			cabine.descendre(modelCapteurEtag_avant);
			sensDeplacement = SensDepEnum.DESCENTE;

		}

		// permet de demander le prochain arret
		else if (etageCible > this.etageCourant) {
			sensDeplacement = SensDepEnum.MONTEE;
			cabine.arret_prochain_arret(modelCapteurEtage_final);

		}

		// permet de demander le prochain arret
		else if (etageCible < this.etageCourant) {
			sensDeplacement = SensDepEnum.DESCENTE;
			cabine.arret_prochain_arret(modelCapteurEtage_final);
		}

		else
			System.out.println("Erreur bouger cabine (model)");

	}

	// signal renvoyé lorsque elle a bougé.
	public void signal() {

		if (sensDeplacement == SensDepEnum.MONTEE) {
			cabineVue.update_position(position_exacte);
			porteVue.update_position(position_exacte + 42);
			capteur_cabine.setLocation(cabineVue.getLocation());

		}

		if (sensDeplacement == SensDepEnum.DESCENTE) {
			cabineVue.update_position(position_exacte);
			porteVue.update_position(position_exacte + 42);
			capteur_cabine.setLocation(cabineVue.getLocation());

		}

	}

	public void appeler(Appel appel) {
		System.out.println("Appel vers " + appel.getEtage() + " envoyé à l'ordonnanceur.");
		ordonnanceur.ajouterAppel(appel);

	}

	public void arreter_d_urgence() {
		if (arret_urgence) {
			arret_urgence = false;
			arret_urgence_btn.setBackground(null);
		}

		else {
			arret_urgence = true;
			arret_urgence_btn.setBackground(Color.RED);
		}

		ordonnanceur.reset();

	}

	public void update_liste_view() {

		// lblListeOrdo.setText(ordonnanceur.toString());
		// utiliser une fonctyion d'affichage de l'ordonnanceur.
	}

	/**
	 * Permet de vérifier si la cabine est présente devant un capteur.
	 */
	public void verify_presence() {

		for (model.CapteurEtage modelCapteurEtage : capteurs.keySet()) {

			view.CapteurEtage viewCapteurEtage = this.capteurs.get(modelCapteurEtage);

			Rectangle rectB = capteur_cabine.getBounds();

			Rectangle result = SwingUtilities.computeIntersection(viewCapteurEtage.getX(), viewCapteurEtage.getY(),
					viewCapteurEtage.getWidth(), viewCapteurEtage.getHeight(), rectB);

			if (result.getWidth() > 0 && result.getHeight() > 0) {
				viewCapteurEtage.SetPresenceOn();
				modelCapteurEtage.setPresenceCabine(true);
				etageCourant = modelCapteurEtage.getEtage();
			} else {
				viewCapteurEtage.SetPresenceOff();
				modelCapteurEtage.setPresenceCabine(false);
			}

		}

	}

	private void ouvrir_porte() {
		if (ok && ok2 && !porte.isStatus() && !arret_urgence) {
			// si on est arrivé

			// ouvrir les portes + temp
			porteVue.set_open();
			porte.ouvrir();
			porte.maintenir();
			porteVue.set_close();
			porte.fermer();
			ok = false;
			ok2 = false;
			// suite = true; // on peut aller vers un autre étage.

		}
	}

	public static void main(String[] args) {

		SystemeControlleur systemeControlleur = new SystemeControlleur();

		while (true) {
			systemeControlleur.controle();
		}

	}

	public int getEtageCourant() {
		return etageCourant;
	}

	public void setEtageCourant(int etageCourant) {
		this.etageCourant = etageCourant;
	}

	public SensDepEnum getSensDeplacement() {
		return sensDeplacement;
	}

	public void setSensDeplacement(SensDepEnum sensDeplacement) {
		this.sensDeplacement = sensDeplacement;
	}

	public Ordonnanceur getOrdonnanceur() {
		return ordonnanceur;
	}

	public void setOrdonnanceur(Ordonnanceur ordonnanceur) {
		this.ordonnanceur = ordonnanceur;
	}

	public Cabine getCabine() {
		return cabine;
	}

	public void setCabine(Cabine cabine) {
		this.cabine = cabine;
	}

	public Porte getPorte() {
		return porte;
	}

	public void setPorte(Porte porte) {
		this.porte = porte;
	}

	@Override
	public String toString() {
		return "SystemeControlleur [etageCourant=" + etageCourant + ", sensDeplacement=" + sensDeplacement
				+ ", ordonnanceur=" + ordonnanceur + ", cabine=" + cabine + ", capteursEtages=" + ", porte=" + porte
				+ "]";
	}

	public int getPosition_exacte() {
		return position_exacte;
	}

	public void setPosition_exacte(int position_exacte) {
		this.position_exacte = position_exacte;
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public boolean isArret_urgence() {
		return arret_urgence;
	}

}
