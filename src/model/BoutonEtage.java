package model;

public class BoutonEtage extends Bouton{
	private int numEtage;
	private SensDepEnum sens;
	
	
	
	
	public BoutonEtage(int numEtage, SensDepEnum sens) {
		super();
		this.numEtage = numEtage;
		this.sens = sens;
	}

	public int getNumEtage() {
		return numEtage;
	}

	public void setNumEtage(int numEtage) {
		this.numEtage = numEtage;
	}

	public SensDepEnum getSens() {
		return sens;
	}

	public void setSens(SensDepEnum sens) {
		this.sens = sens;
	}

	
	
	
	
	
	
	
	
}
