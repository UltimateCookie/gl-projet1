package model;

public class Porte {

	boolean status;

	public Porte() {
		this.status = false;
	}

	public void ouvrir() {
		System.out.println("PORTE: ouvrir");
		status = true;
	}

	public void fermer() {
		System.out.println("PORTE: fermer");
		status = false;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public void maintenir() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
