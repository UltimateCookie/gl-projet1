package model;

import java.util.Timer;
import java.util.TimerTask;

public class Cabine {

	private SystemeControlleur systemeControlleur;

	public Cabine(SystemeControlleur s) {
		this.systemeControlleur = s;
	}

	public void monter(CapteurEtage modelCapteurEtage) {
		System.out.println("montée de la cabine");

		long temps = 60; // délai avant de répéter la tache 
		long startTime = 0; // délai avant la mise en route
		Timer timer = new Timer(); 
		TimerTask tache = new TimerTask() { 
											
			@Override
			public void run() {

				systemeControlleur.setPosition_exacte(systemeControlleur.getPosition_exacte() - 1);
				systemeControlleur.signal();

				if (modelCapteurEtage.presenceCabine || systemeControlleur.isArret_urgence()) {
					timer.cancel();
					timer.purge();
					systemeControlleur.setSensDeplacement(SensDepEnum.ARRET);
				}

			}

		};
		timer.scheduleAtFixedRate(tache, startTime, temps);

	}

	public void descendre(CapteurEtage modelCapteurEtage) {
		System.out.println("descente de la cabine");

		long temps = 60; 
		long startTime = 0; 
		Timer timer = new Timer();
		TimerTask tache = new TimerTask() { 
											
			@Override
			public void run() {

				systemeControlleur.setPosition_exacte(systemeControlleur.getPosition_exacte() + 1);
				systemeControlleur.signal();

				if (modelCapteurEtage.presenceCabine || systemeControlleur.isArret_urgence()) {
					timer.cancel();
					timer.purge();
					systemeControlleur.setSensDeplacement(SensDepEnum.ARRET);
				}

			}

		};
		timer.scheduleAtFixedRate(tache, startTime, temps);

	}

	public void arret_prochain_arret(CapteurEtage modelCapteurEtage) {

		System.out.println("CABINE: Arret prochain étage");
		if (systemeControlleur.getSensDeplacement() == SensDepEnum.MONTEE) {
			monter(modelCapteurEtage);
		} else if (systemeControlleur.getSensDeplacement() == SensDepEnum.DESCENTE) {
			descendre(modelCapteurEtage);
		}

	}

	public void arret_urgence() {
		systemeControlleur.setSensDeplacement(SensDepEnum.ARRET);
	}

}
