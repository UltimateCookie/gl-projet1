package model;

public class CapteurEtage {
	
	boolean presenceCabine;
	int etage;
	
	public CapteurEtage(int etage) {
		this.presenceCabine = false;
		this.etage = etage;
	}
	
	public void setPresenceCabine(boolean status) {
		this.presenceCabine = status;
	}

	public int getEtage() {
		return etage;
	}

}
