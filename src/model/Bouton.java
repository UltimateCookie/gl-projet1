package model;

public abstract class Bouton {
	protected boolean etat;
	
	
	public void changeEtat(boolean etat){
		this.etat = etat;
	}


	public boolean isEtat() {
		return etat;
	}


	public void setEtat(boolean etat) {
		this.etat = etat;
	}
	
	
	
}
