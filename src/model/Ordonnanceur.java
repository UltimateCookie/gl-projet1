package model;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class Ordonnanceur {

	private List<Appel> appels;
	private SystemeControlleur systemeControlleur;
	private List<Appel> prochainesDep;

	private TreeSet<Appel> prochaineDepMontee = new TreeSet<>(); // floors above
																	// currentFloor
	private TreeSet<Appel> prochaineDepDesc = new TreeSet<>(); // floors below
																// currentFloor

	public Ordonnanceur(SystemeControlleur cabineControlleur) {
		this.systemeControlleur = cabineControlleur;
		this.appels = new ArrayList<>();
		this.prochainesDep = new ArrayList<>();
	}

	public void ajouterAppel(Appel a) {
		if (a.getEtage() >= systemeControlleur.getEtageCourant() && a.getSens() == SensDepEnum.MONTEE) {
			prochaineDepMontee.add(a);
		} else if (a.getEtage() <= systemeControlleur.getEtageCourant() && a.getSens() == SensDepEnum.DESCENTE) {
			prochaineDepDesc.add(a);
		} else {
			if (a.getSens() == SensDepEnum.DESCENTE) {
				prochaineDepDesc.add(a);
			} else {
				prochaineDepMontee.add(a);
			}
		}
	}

	public Appel recupererAppel() {

		if (systemeControlleur.getSensDeplacement() == SensDepEnum.DESCENTE) {
			return prochaineDepDesc.pollLast(); // highest floor in down, or
												// null if empty
		} else {
			if (systemeControlleur.getSensDeplacement() == SensDepEnum.MONTEE)
				return prochaineDepMontee.pollFirst(); // lowest floor in up, or
														// null if empty
			else {
				if (!this.prochaineDepMontee.isEmpty())
					return prochaineDepMontee.pollFirst();
				else if (!this.prochaineDepDesc.isEmpty())
					return prochaineDepDesc.pollLast();

			}

		}
		return null;
	}

	/*
	 * public void ajouterAppel(Appel appel) { this.appels.add(appel); }
	 */

	public void ordonnerAppels() {
		Appel appel_prioritaire;
		if (systemeControlleur.getSensDeplacement() == SensDepEnum.ARRET && this.appels.size() == 1) { // la
																										// cabine
																										// est
																										// arret�e/liste
																										// des
																										// appels
																										// contient
																										// une
																										// seul
																										// demande
			this.prochainesDep.add(this.appels.get(0)); // on r�cup�re le
														// premier �l�ment
														// puisque c'est le seul
														// qu'on a
			this.appels.clear();
		} else {
			if (systemeControlleur.getSensDeplacement() == SensDepEnum.MONTEE) { // si
																					// la
																					// cabine
																					// est
																					// en
																					// mont�e

				for (int i = 0; i < appels.size(); i++) {

					if (appels.get(i).getEtage() > systemeControlleur.getEtageCourant()) {
						appel_prioritaire = appels.get(i);
						for (int j = i + 1; j < appels.size(); j++) {
							if (appels.get(j).getEtage() < appel_prioritaire.getEtage())
								appel_prioritaire = appels.get(j);
						}
						this.prochainesDep.add(appel_prioritaire);
						this.appels.remove(appel_prioritaire);
					}
				}

			} else {
				while (premierDemandeDescente() != null) {
					appel_prioritaire = premierDemandeDescente(); // car la
																	// m�thode
																	// renvoi la
																	// premier
																	// demande
																	// enregistr�e
																	// sur ce
																	// sens ceci
																	// va nous
																	// servir
																	// pour
																	// initialis�
																	// la
																	// variable
																	// pour
																	// effectuer
																	// les tests
																	// ci-dessous

					for (Appel a : appels) { // on parcour la liste pour
												// extraire les appels qui ont
												// le m�me sens de d�placement
												// que la cabine
						if (a.getSens() == SensDepEnum.DESCENTE) {
							if (systemeControlleur.getEtageCourant() > a.getEtage()) { // puis
																						// on
																						// cherche
																						// l'appel
																						// le
																						// plus
																						// proche
																						// �
																						// la
																						// position
																						// de
																						// la
																						// cabine
																						// avec
																						// un
																						// interval
																						// de
																						// 1
																						// car
																						// la
																						// cabine
																						// re�oit
																						// l'ordre
																						// de
																						// s'arreter
																						// lorsqu'il
																						// arrive
																						// au
																						// niveau
																						// qui
																						// pr�c�de
																						// l'arret
																						// final
								if (a.getEtage() > appel_prioritaire.getEtage()) { // s'il
																					// y
																					// a
																					// un
																					// appel
																					// plus
																					// proche
																					// du
																					// position
																					// de
																					// la
																					// cabine,
																					// il
																					// est
																					// donc
																					// le
																					// plus
																					// prioritaire
									appel_prioritaire = a;
								}
							}
						}

					}

					this.appels.remove(appel_prioritaire);
					this.prochainesDep.add(appel_prioritaire);
				}
			}
		}

		// this.appels.clear(); // on vide la liste des appels
	}


	private Appel premierDemandeMontee() {
		for (Appel a : appels) {
			if (a.getSens() == SensDepEnum.MONTEE)
				return a;
		}
		return null;
	}

	private Appel premierDemandeDescente() {
		for (Appel a : appels) {
			if (a.getEtage() < systemeControlleur.getEtageCourant() && a.getSens() == SensDepEnum.DESCENTE)
				return a;
		}
		return null;
	}

	public List<Appel> getProchainesDep() {
		return prochainesDep;
	}

	public void setProchainesDep(List<Appel> prochainesDep) {
		this.prochainesDep = prochainesDep;
	}

	public List<Appel> getAppels() {
		return appels;
	}

	public void setAppels(List<Appel> appels) {
		this.appels = appels;
	}

	public boolean isEmpty() {
		return appels.isEmpty();
	}

	public void display_size() {
		System.out.println("ordo taille:" + appels.size());
	}

	public TreeSet<Appel> getProchaineDepMontee() {
		return prochaineDepMontee;
	}

	public void setProchaineDepMontee(TreeSet<Appel> prochaineDepMontee) {
		this.prochaineDepMontee = prochaineDepMontee;
	}

	public TreeSet<Appel> getProchaineDepDesc() {
		return prochaineDepDesc;
	}

	public void setProchaineDepDesc(TreeSet<Appel> prochaineDepDesc) {
		this.prochaineDepDesc = prochaineDepDesc;
	}

	public void reset() {
		appels.clear();
		prochainesDep.clear();
		prochaineDepDesc.clear();
		prochaineDepMontee.clear();
	}

}
