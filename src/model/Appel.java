package model;

public class Appel implements Comparable<Appel> {

	private int etage;
	private SensDepEnum sens;

	public Appel(int etage, SensDepEnum sens) {

		this.etage = etage;
		this.sens = sens;

	}

	public Appel(int etage) {

		this.etage = etage;
		this.sens = null;

	}

	public int getEtage() {
		return etage;
	}

	public void setEtage(int etage) {
		this.etage = etage;
	}

	public SensDepEnum getSens() {
		return sens;
	}

	public void setSens(SensDepEnum sens) {
		this.sens = sens;
	}

	@Override
	public String toString() {
		return "Appel [etage=" + etage + ", sens=" + sens + "]";
	}

	@Override
	public int compareTo(Appel o) {
		if (this.getEtage() < o.getEtage())
			return -1;
		if (this.getEtage() > o.getEtage())
			return 1;
		return 0;
	}

}
