package view;

import java.awt.Color;
import java.awt.Point;

import javax.swing.JPanel;

public class CapteurEtage extends JPanel {

	private String name;
	private Point p;

	public CapteurEtage(String name, Point p) {
		super();
		this.name = name;
		this.p = p;

		SetPresenceOff();
		this.setBounds(p.x, p.y, 20, 20);
		if(name.equals("capteur cabine"))
			this.setBounds(p.x, p.y, 20, 20);

	}

	public void SetPresenceOn() {
		if(name.equals("Capteur cabine"))
			this.setBackground(Color.YELLOW);
		else
			this.setBackground(Color.RED);
	}

	public void SetPresenceOff() {
		if(name.equals("Capteur cabine"))
			this.setBackground(Color.BLACK);
		else
			this.setBackground(Color.GRAY);
	}

	public String getName() {
		return name;
	}

	public void update_position(int y) {
		this.p.y = y;
		this.setLocation(p);
		
	}

}
