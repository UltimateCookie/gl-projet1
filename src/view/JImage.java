package view;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class JImage extends JLabel {

	public JImage() {
		super();
	}

	public JImage(ImageIcon imageIcon) {
		super(imageIcon);
	}

}
