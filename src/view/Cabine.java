package view;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Cabine extends JPanel {

	private JImage imgCabine;
	private Point p;

	public Cabine() {
		super();

		this.setBackground(Color.WHITE);
		p = new Point(452, 672);
		this.setBounds(p.x, p.y, 100, 100);

		BufferedImage bufferedImgCabine;

		try {

			bufferedImgCabine = ImageIO.read(new File(ImgPath.CABINE.toString()));
			this.imgCabine = new JImage(new ImageIcon(bufferedImgCabine));
			this.setBounds(p.x, p.y, bufferedImgCabine.getWidth(), bufferedImgCabine.getHeight() + 5);
			this.add(this.imgCabine);

		} catch (IOException e) {

			System.out.println("Erreur: Image Cabine.png manquante");
			this.setBackground(Color.RED);
			this.add(new JLabel("Cabine.png manquante"));
			// e.printStackTrace();

		}

	}

	public void update_position(int y) {
		this.p.y = y;
		this.setLocation(p);
	}

	/*
	 * public void affichage_console_cabine() {
	 * 
	 * System.out.println("| . |");
	 * 
	 * }
	 */

}
