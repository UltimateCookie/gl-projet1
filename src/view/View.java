package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import model.Appel;

public class View extends JFrame {

	private JMenuBar menuBar;
	private JLayeredPane background;
	private JPanel barreDeDroite;
	private JLabel labelBarreDeDroite;
	private Vector<CapteurEtage> capteurs;

	private Cabine cabine;
	private Porte porte;
	private JButton btnNewButton_0;
	private JButton btnNewButton_1;
	private JButton btnNewButton_2;
	private JButton btnNewButton_3;
	private JButton btnNewButton_4;
	private JLabel lblListeOrdo;
	private CapteurEtage capteur_cabine;

	private model.SystemeControlleur systemeControlleur;
	private JButton btnNewButton_arret_urgence;

	public View(model.SystemeControlleur systemeControlleur) {
		super();
		this.systemeControlleur = systemeControlleur;
		// init contentPane
		this.getContentPane().setBackground(Color.WHITE);
		this.getContentPane().setLayout(null);

		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setBounds(100, 100, 1200, 900);

		this.repaint();
		this.revalidate();
		System.out.println("Init contentPane: OK");

		// init background
		this.background = new JLayeredPane();
		this.background.setBounds(20, 25, 1000, 813);
		this.getContentPane().add(this.background);
		displayBackgroundImage();

		this.repaint();
		this.revalidate();
		System.out.println("Init background: OK");

		// init barre de droite
		this.barreDeDroite = new JPanel();
		this.barreDeDroite.setBounds(1041, 25, 150, 813);
		this.barreDeDroite.setBackground(Color.WHITE);

		barreDeDroite.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		// init barre de droite: label
		this.labelBarreDeDroite = new JLabel("Boutons et états");
		this.labelBarreDeDroite.setFont(new Font("Arimo", Font.PLAIN, 14));

		barreDeDroite.add(labelBarreDeDroite);

		btnNewButton_0 = new JButton("Etage 0");
		barreDeDroite.add(btnNewButton_0);
		btnNewButton_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Bouton Etage 0 cliqué");
				systemeControlleur.appeler(new Appel(0));
			}
		});

		btnNewButton_1 = new JButton("Etage 1");
		barreDeDroite.add(btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Bouton Etage 1 cliqué");
				systemeControlleur.appeler(new Appel(1));
			}
		});

		btnNewButton_2 = new JButton("Etage 2");
		barreDeDroite.add(btnNewButton_2);
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Bouton Etage 2 cliqué");
				systemeControlleur.appeler(new Appel(2));
			}
		});

		btnNewButton_3 = new JButton("Etage 3");
		barreDeDroite.add(btnNewButton_3);
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Bouton Etage 3 cliqué");
				systemeControlleur.appeler(new Appel(3));
			}
		});

		btnNewButton_4 = new JButton("Etage 4");
		barreDeDroite.add(btnNewButton_4);
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Bouton Etage 4 cliqué");
				systemeControlleur.appeler(new Appel(4));
			}
		});

		btnNewButton_arret_urgence = new JButton("Arret d'urgence");
		barreDeDroite.add(btnNewButton_arret_urgence);
		btnNewButton_arret_urgence.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Bouton Arret d'urgence cliqué");
				systemeControlleur.arreter_d_urgence();
			}
		});

		this.getContentPane().add(barreDeDroite);

		lblListeOrdo = new JLabel("Liste d'appel:");
		// barreDeDroite.add(lblListeOrdo);

		this.repaint();
		this.revalidate();
		System.out.println("Init barreDeDroite: OK");

		// init menu
		this.menuBar = new JMenuBar();
		this.menuBar.setBounds(0, 0, 1250, 25);
		this.getContentPane().add(this.menuBar);
		displayMenu();

		System.out.println("Init menuBar: OK");

		// init cabine
		this.cabine = new Cabine();
		this.capteur_cabine = new CapteurEtage("capteur cabine", cabine.getLocation());
		capteur_cabine.setBackground(Color.BLACK);
		this.background.add(this.cabine);
		this.background.setLayer(this.cabine, 1);
		this.background.add(this.capteur_cabine);
		this.background.setLayer(this.capteur_cabine, 3);

		this.repaint();
		this.revalidate();
		System.out.println("Init cabine: OK");

		// init portes
		this.porte = new Porte();
		this.background.add(this.porte);
		this.background.setLayer(this.porte, 2);

		System.out.println("Init Porte: OK");
		this.revalidate();

		// init boutons
		init_boutons();

		this.revalidate();
		System.out.println("Init boutons: OK");

		// init capteurs
		this.capteurs = new Vector<CapteurEtage>();
		capteurs.add(new CapteurEtage("Etage 0", new Point(450, 680)));
		capteurs.add(new CapteurEtage("Etage 1", new Point(450, 510)));
		capteurs.add(new CapteurEtage("Etage 2", new Point(450, 340)));
		capteurs.add(new CapteurEtage("Etage 3", new Point(450, 210)));
		capteurs.add(new CapteurEtage("Etage 4", new Point(450, 80)));

		for (CapteurEtage capteurEtage : this.capteurs) {
			this.background.add(capteurEtage);
			this.background.setLayer(capteurEtage, 2);
			System.out.println("Init capteurEtage-" + capteurEtage.getName() + ": OK");
		}

		this.repaint();
		this.revalidate();

	}

	/**
	 * Permet d'afficher l'image sur background
	 */
	public void displayBackgroundImage() {
		JPanel panelBackground = new JPanel(new BorderLayout());
		panelBackground.setBackground(Color.WHITE);
		panelBackground.setBounds(0, 0, 1000, 813);

		try {

			BufferedImage bufferedImgBackground = ImageIO.read(new File(ImgPath.BACKGROUND.toString()));
			JImage imgBackground = new JImage(new ImageIcon(bufferedImgBackground));
			imgBackground.setHorizontalAlignment(SwingConstants.CENTER);

			panelBackground.add(imgBackground, BorderLayout.CENTER);

		} catch (IOException e) {
			System.out.println("Erreur: Image background.png manquante");
			panelBackground.setBackground(Color.RED);
			panelBackground.add(new JLabel("Erreur: Image background.png manquante"));
			// e.printStackTrace();
		}
		this.background.setLayer(panelBackground, 0);
		this.background.add(panelBackground);
		this.repaint();
		this.revalidate();
	}

	/**
	 * Permet d'afficher les menus
	 */
	public void displayMenu() {

		// Menu action
		JMenu menuAction = new JMenu("Actions");
		menuAction.setFont(new Font("Arimo", Font.PLAIN, 14));

		// Menu action: Exit.
		JMenuItem menuAction_Quitter = new JMenuItem("Quitter");
		menuAction_Quitter.setFont(new Font("Arimo", Font.PLAIN, 14));
		menuAction_Quitter.addActionListener(new ActionListener() {

			@SuppressWarnings("static-access")
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane jop = new JOptionPane();
				int choix = jop.showConfirmDialog(null, "Voulez-vous quitter ?", "Quitter", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
				if (choix == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});

		JMenu menuPlus = new JMenu("Plus");
		menuPlus.setFont(new Font("Arimo", Font.PLAIN, 14));

		JMenuItem menuPlus_Infos = new JMenuItem("Informations");
		menuPlus_Infos.setFont(new Font("Arimo", Font.PLAIN, 14));

		menuPlus_Infos.addActionListener(new ActionListener() {

			@SuppressWarnings("static-access")
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane jop1 = new JOptionPane();
				String str = "Fait par: Loic, Nadia, Ishac, Abdelilah";
				jop1.showMessageDialog(null, str, "Informations", JOptionPane.INFORMATION_MESSAGE);

			}
		});

		menuAction.add(menuAction_Quitter);
		menuPlus.add(menuPlus_Infos);
		this.menuBar.add(menuAction);
		this.menuBar.add(menuPlus);
		this.repaint();
		this.revalidate();
	}

	private void init_boutons() {
		JButton monter_etage0 = new JButton("Monter");
		monter_etage0.setBounds(600, 700, 117, 25);
		background.add(monter_etage0);
		this.background.setLayer(monter_etage0, 2);
		monter_etage0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Bouton extérieur - Etage 0 montée - cliqué");
				systemeControlleur.appeler(new Appel(0, model.SensDepEnum.MONTEE));
			}
		});

		JButton monter_etage1_up = new JButton("Monter");
		monter_etage1_up.setBounds(600, 525, 117, 25);
		background.add(monter_etage1_up);
		this.background.setLayer(monter_etage1_up, 2);
		monter_etage1_up.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Bouton extérieur - Etage 1 montée - cliqué");
				systemeControlleur.appeler(new Appel(1, model.SensDepEnum.MONTEE));
			}
		});

		JButton monter_etage1_down = new JButton("Descendre");
		monter_etage1_down.setBounds(600, 575, 117, 25);
		background.add(monter_etage1_down);
		this.background.setLayer(monter_etage1_down, 2);
		monter_etage1_down.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Bouton extérieur - Etage 1 descente - cliqué");
				systemeControlleur.appeler(new Appel(1, model.SensDepEnum.DESCENTE));
			}
		});

		JButton monter_etage2_up = new JButton("Monter");
		monter_etage2_up.setBounds(600, 375, 117, 25);
		background.add(monter_etage2_up);
		this.background.setLayer(monter_etage2_up, 2);
		monter_etage2_up.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Bouton extérieur - Etage 2 montée - cliqué");
				systemeControlleur.appeler(new Appel(2, model.SensDepEnum.MONTEE));
			}
		});

		JButton monter_etage2_down = new JButton("Descendre");
		monter_etage2_down.setBounds(600, 425, 117, 25);
		background.add(monter_etage2_down);
		this.background.setLayer(monter_etage2_down, 2);
		monter_etage2_down.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Bouton extérieur - Etage 2 descente - cliqué");
				systemeControlleur.appeler(new Appel(2, model.SensDepEnum.DESCENTE));
			}
		});

		JButton monter_etage3_up = new JButton("Monter");
		monter_etage3_up.setBounds(600, 225, 117, 25);
		background.add(monter_etage3_up);
		this.background.setLayer(monter_etage3_up, 2);
		monter_etage3_up.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Bouton extérieur - Etage 3 montée - cliqué");
				systemeControlleur.appeler(new Appel(3, model.SensDepEnum.MONTEE));
			}
		});

		JButton monter_etage3_down = new JButton("Descendre");
		monter_etage3_down.setBounds(600, 275, 117, 25);
		background.add(monter_etage3_down);
		this.background.setLayer(monter_etage3_down, 2);
		monter_etage3_down.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Bouton extérieur - Etage 3 descente - cliqué");
				systemeControlleur.appeler(new Appel(3, model.SensDepEnum.DESCENTE));
			}
		});

		JButton monter_etage4_down = new JButton("Descendre");
		monter_etage4_down.setBounds(600, 75, 117, 25);
		background.add(monter_etage4_down);
		this.background.setLayer(monter_etage4_down, 2);
		monter_etage4_down.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Bouton extérieur - Etage 4 descente - cliqué");
				systemeControlleur.appeler(new Appel(4, model.SensDepEnum.DESCENTE));
			}
		});
		this.revalidate();

	}

	public Cabine getCabine() {
		return cabine;
	}

	public Vector<CapteurEtage> getCapteurs() {
		return capteurs;
	}

	public void repaintVue() {
		this.repaint();
		this.revalidate();
	}

	public Porte getPorte() {
		return porte;
	}

	public JLabel getLblListeOrdo() {
		return lblListeOrdo;
	}

	public void setLblListeOrdo(JLabel lblListeOrdo) {
		this.lblListeOrdo = lblListeOrdo;
	}

	public CapteurEtage getCapteur_cabine() {
		return capteur_cabine;
	}

	public JButton getBtnNewButton_arret_urgence() {
		return btnNewButton_arret_urgence;
	}
}
