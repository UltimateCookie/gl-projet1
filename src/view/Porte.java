package view;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Porte extends JPanel {

	private JImage imgPorte;
	private Point p;

	public Porte() {
		super();

		this.setBackground(Color.WHITE);
		p = new Point(502, 702);
		this.setBounds(p.x, p.y, 100, 100);

		BufferedImage bufferedImgCabine;

		set_close();

	}

	public void update_position(int y) {
		this.p.y = y;
		this.setLocation(p);
	}

	public void set_open() {
		try {

			BufferedImage bufferedImgCabine = ImageIO.read(new File(ImgPath.PORTE_OUVERTE.toString()));
			this.imgPorte = new JImage(new ImageIcon(bufferedImgCabine));
			this.removeAll();
			this.setBounds(p.x, p.y, bufferedImgCabine.getWidth(), bufferedImgCabine.getHeight() + 5);
			this.add(this.imgPorte);
			this.repaint();
			this.revalidate();

		} catch (IOException e) {

			System.out.println("Erreur: Image Porte.png manquante");
			this.setBackground(Color.RED);
			this.add(new JLabel("Porte.png manquante"));
			// e.printStackTrace();

		}
	}

	public void set_close() {
		try {

			BufferedImage bufferedImgCabine = ImageIO.read(new File(ImgPath.PORTE_FERMEE.toString()));
			this.imgPorte = new JImage(new ImageIcon(bufferedImgCabine));
			this.removeAll();
			this.setBounds(p.x, p.y, bufferedImgCabine.getWidth(), bufferedImgCabine.getHeight() + 5);
			this.add(this.imgPorte);

		} catch (IOException e) {

			System.out.println("Erreur: Image Porte.png manquante");
			this.setBackground(Color.RED);
			this.add(new JLabel("Porte.png manquante"));
			// e.printStackTrace();

		}
		this.repaint();
		this.revalidate();
	}

}
