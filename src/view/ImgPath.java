package view;

//A rajouter dans la conception
public enum ImgPath {

	BACKGROUND {
		public String toString() {
			return "res/img/Background.png";
		}
	},

	CABINE {
		public String toString() {
			return "res/img/Cabine.png";
		}
	},
	
	PORTE_OUVERTE {
		public String toString() {
			return "res/img/Porte_ouverte.png";
		}
	},
	
	PORTE_FERMEE {
		public String toString() {
			return "res/img/Porte_fermee.png";
		}
	}

}
